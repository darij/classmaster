class Index extends CFormWidget
	@init 'index'
		
	theme_onchange: -> Editor.setTheme "ace/theme/" + @$theme.val()
	syntax_onchange: -> Editor.getSession().setMode "ace/mode/"+@$syntax.val()
	
	prj_onclick: (e) -> @advanced.toggle(); off
	