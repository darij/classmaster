CREATE TABLE options(
id int primary key,
theme varchar(255) not null,
syntax varchar(255) not null
) ENGINE=InnoDB;